entity selector is

Port(salida1,salida2,salida3,salida4:in STD_LOGIC_VECTOR (3 downto 0);
selec:in STD_LOGC_VECTOR (4 downto 0);
valor:out STD_LOGIC_VECTOR(3 downto 0));
end selector;

architecture behavioral of selector is

begin 

with selec select


valor<= salida1 when "11110",
salida2 when "11101",
salida3 when "01011",
salida4 when others ;

end behavioral;