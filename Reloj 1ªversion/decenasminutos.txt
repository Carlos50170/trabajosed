entity decminutos is

Port(digito3:in STD_LOGIC;
cont4:inout STD_LOGIC_VECTOR(3 downto 0);
salida4 :out STD_LOGIC_VECTOR (3 downto 0);
end decminutos;

architecture behavioral of decminutos is

begin 

process(digito3)
   begin
        if(digito3'event and digito3='1')then

cont4<=cont4+1;

salida4<=cont4;

if cont4="1001" then

cont4<="0000";

end if;

end if;

end process;

end Behavioral;