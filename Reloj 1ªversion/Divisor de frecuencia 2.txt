entity divisor2 is

Port (clk2: in STD_LOGIC;

cont:inout STD_LOGIC_VECTOR (1 downto 0);

clk3:out STD_LOGIC;

selec:out STD_LOGIC_VECTOR(4 downto 0));

end divisor2;



architecture Behavioral of dos is

begin

   Procces(clk2)
        begin 
          if(clk2'event and clk2='1')then
             cont<=cont+1;
             if cont="11" then
                clk3<='1';
                else
                   
clk3<='0';
end if;


case cont is 

when "00"=>(select)<="11110";
when "01"=>(select)<="11101";
when "10"=>(select)<="01011";
when "11"=>(select)<="10111";
when others=>null;

end case;
end if
end process;
end Behavioral;